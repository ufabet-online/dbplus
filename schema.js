module.exports = {
    make: (fields, action) => {
        const schema = {}
        Object.keys(fields).forEach(fname => {
            const f = fields[fname]
            if (f.options[action] === false) {
                return;
            }

            if (action !== 'view' && f.options.allowedEmpty === true) {
                schema[fname] = {
                    anyOf: [
                        { ...f.base },
                        { type: 'null' },
                        { type: 'string', maxLength: 0 },
                    ],
                    description: f.options.description  || "",
                    ...(f.options.props || {}),
                }

                return;
            }

            schema[fname] = { ...f.base, ...(f.options.props || {}), description: f.options.description || "", };
        })

        return schema;
    },
    string: (options = {}) => ({ base: { type: 'string' }, options }),
    integer: (options = {}) => ({ base: { type: 'number' }, options }),
    number: (options = {}) => ({ base: { type: 'number' }, options }),
    dateTime: (options = {}) => ({ base: { type: 'string', format: 'date-time' }, options }),
    boolean: (options = {}) => ({ base: { type: 'boolean' }, options }),
    raw: (sch, options = {}) => ({ base: { ...sch }, options }),
    object: (sch, options = {}) => ({
        base: {
            type: 'object',
            properties: sch,
            additionalProperties: false,
        },
        options,
    }),
}
  