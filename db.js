const dayjs = require('dayjs');
const debug = require('debug')('dbplus');

const byIds = (f) => {
  if (f.id && Array.isArray(f.id)) {
    return f.id.length
  }

  return null;
}

module.exports.list = async (q, { filter, range, sort }, { search, where, result }) => {
  const _filter = JSON.parse(filter || '{}');
  const _sort = JSON.parse(sort || '["id", "DESC"]');
  const _range = JSON.parse(range || `[0,${ byIds(_filter) || 30}]`);

  if (_filter.q) {
    _filter.search = _filter.q;
    delete(_filter.q)
  }

  q.where(function() {
      if (where) {
        where(this)
      }

      Object.keys(_filter).forEach(w => {
          if (w === 'search') {
              this.andWhere(function() {
                  search.forEach((k) => {
                      this.orWhere(k, 'LIKE', `%${_filter['search'].replace(/([\"\'\$\%]+)/, '')}%`)
                  })
              });
          } else {
            if (Array.isArray(_filter[w])) {
              this.whereIn(w, _filter[w])
            } else
            if ((typeof _filter[w]) === 'object'){
              if (_filter[w].min) {
                this.andWhere(w, '>=' , _filter[w].min)
              }
              if (_filter[w].max) {
                this.andWhere(w, '<=' , _filter[w].max)
              }
              if (_filter[w].begin) {
                // console.log(_filter[w].begin, dayjs(_filter[w].begin).format('YYYY-MM-DD HH:mm:ss:000'))
                this.andWhere(w, '>=' , dayjs(_filter[w].begin).format(process.env.SPZ_DB_DATE_FORMAT || 'YYYY-MM-DD HH:mm:ss:000'))
              }
              if (_filter[w].end) {
                // console.log(_filter[w].end, dayjs(_filter[w].end).format('YYYY-MM-DD HH:mm:ss:999'))
                this.andWhere(w, '<=' , dayjs(_filter[w].end).format(process.env.SPZ_DB_DATE_FORMAT || 'YYYY-MM-DD HH:mm:ss:999'))
              }
              if (_filter[w].before) {
                // console.log(_filter[w].end, dayjs(_filter[w].end).format('YYYY-MM-DD HH:mm:ss:999'))
                this.andWhere(w, '<' , dayjs(_filter[w].before).format(process.env.SPZ_DB_DATE_FORMAT || 'YYYY-MM-DD HH:mm:ss:000'))
              }
              if (_filter[w].after) {
                // console.log(_filter[w].end, dayjs(_filter[w].end).format('YYYY-MM-DD HH:mm:ss:999'))
                this.andWhere(w, '>' , dayjs(_filter[w].after).format(process.env.SPZ_DB_DATE_FORMAT || 'YYYY-MM-DD HH:mm:ss:000'))
              }
            } else {
              this.andWhere(w, _filter[w])
            }
          }
      })
  })
  
  debug(await q.clone().toSQL().toNative());

  const data = await q.clone().offset(_range[0]).limit(_range[1] - _range[0]).orderBy(..._sort)
  const total = await q.clone().count('* AS C').then(c => c[0].C);  

  return {
      total,
      range: `${_range.join('-')}/${total}`,
      data: result ? result(data) : data,
  }
}
