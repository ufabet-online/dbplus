const db = require("./db")
const schema = require("./schema")

module.exports = {
    ...schema,
    listQuery: db.list,
}

